import { ILanguage } from './../../core/common/model/language.model';
import { CoreService } from './../../core/common/service/core.service';
import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-footer-auth',
  templateUrl: './footer-auth.component.html',
  styleUrls: ['./footer-auth.component.scss']
})
export class FooterAuthComponent implements OnInit {

  imgPath: string = './assets/img/iran.png';
  langText: string = 'فارسی';
  public languages: ILanguage[];

  showUserTypeAction:boolean;
  routeChange: string;

  constructor(private route: Router,
              private translateService: TranslateService,
              private coreService: CoreService) {
                
               }

  ngOnInit(): void {
    this.routeChange = this.route.url;
    this.changeTextRoute(this.routeChange);
    this.switchLanguage(this.coreService.defaultLocaleLang);
    this.languages = this.coreService.getLanguage();
  }

  changeTextRoute(url:string){
    this.showUserTypeAction = false;
    if(url === "/login"){
      this.showUserTypeAction = true;
    }
  }
  
  switchLanguage(language: string) {
    this.translateService.use(language);
    if(language === 'en') {
       this.imgPath = "./assets/img/us-flag.svg";
       this.langText= "English";
       localStorage.setItem("selectedLang", 'en');
      } else if(language === 'fa') {
        this.imgPath = "./assets/img/iran.png";     
        this.langText= "فارسی";
        localStorage.setItem("selectedLang", 'fa');
    }
  }


}
