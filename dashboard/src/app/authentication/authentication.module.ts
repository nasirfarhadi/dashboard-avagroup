import { WebpackTranslateLoader } from './../core/common/service/translate';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { RegisterComponent } from './register/register.component';
import { MultiStepFormComponent } from './register/multi-step-form/multi-step-form.component';
import { StepFormSelectorComponent } from './register/step-form-selector/step-form-selector.component';
import { HeadAuthComponent } from './head-auth/head-auth.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterGetEmailComponent } from './register/multi-step-form/register-get-email/register-get-email.component';
import { RegisterConfirmEmailComponent } from './register/multi-step-form/register-confirm-email/register-confirm-email.component';
import { RegisterInfoUserComponent } from './register/multi-step-form/register-info-user/register-info-user.component';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { LoginComponent } from './login/login.component';
import { FooterAuthComponent } from './footer-auth/footer-auth.component';
import { CoreModule } from '../core/core.module';
import { TranslateModule } from '@ngx-translate/core';
const maskConfig: Partial<IConfig> = {
  validation: true,
};


@NgModule({
  declarations: [RegisterComponent, MultiStepFormComponent, StepFormSelectorComponent, HeadAuthComponent, RegisterGetEmailComponent, RegisterConfirmEmailComponent, RegisterInfoUserComponent, LoginComponent, FooterAuthComponent],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    FormsModule,    
    ReactiveFormsModule,
    NgxMaskModule.forRoot(maskConfig),
    SharedModule,
    CoreModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useClass: WebpackTranslateLoader
      }
    })
  ],
  exports:[
    RegisterComponent
  ]
})
export class AuthenticationModule { }
