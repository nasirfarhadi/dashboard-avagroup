import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Credentials } from 'src/app/core/common/model/credential.model';
import { AuthService } from 'src/app/core/common/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  model: Credentials;
  error = "";
  returnUrl: string;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['',[Validators.required,Validators.email]],
      password: ['',[Validators.required]],
    });
    // get the return url from route parameters
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"];

  }

  onSubmit(e){
    e.preventDefault();
    this.model = {
      username: this.loginForm.controls.email.value,
      password: this.loginForm.controls.password.value,
      success: true,
      message: "",
      result : {
        access_token: "",
        refresh_token: "",
        expireDateTime: ""
      }
    }
    this.authService.login(this.model)
      .subscribe(isLoggedIn => {
        if (isLoggedIn) {
          if (this.returnUrl) {
            this.router.navigate([this.returnUrl]);
          } else {
            this.router.navigate(["/dashboard"]);
          }
        }
      },
      (error: HttpErrorResponse) => {
        console.log("Login error", error);
        if (error.status === 401) {
          this.error = "Invalid User name or Password. Please try again.";
        } else {
          this.error = `${error.statusText}: ${error.message}`;
        }
      });    
  }

}
