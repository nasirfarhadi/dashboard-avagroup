import { Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-head-auth',
  templateUrl: './head-auth.component.html',
  styleUrls: ['./head-auth.component.scss']
})
export class HeadAuthComponent implements OnInit,OnDestroy {
  titlePage:string;
  titleSubscribe: Subscription;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.titleSubscribe = this.route.data.subscribe(
      res=>{
        this.titlePage = res['some_data']
      } 
    );
  }

  ngOnDestroy() {
    this.titleSubscribe.unsubscribe();
  }


}
