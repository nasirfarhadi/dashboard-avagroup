
export interface IResult {
    verifyCodeLength: number;
    verifyCodeExpireTime_Sec: number;
    hashCode: string;
}

export interface IResponse {
    success: boolean;
    message: string;
    result: IResult;
}
