import { IResponse } from './../response.model';
export interface IEmail extends IResponse {
  email: string;
}