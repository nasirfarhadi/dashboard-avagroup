import { IEmail } from './email-user.model';
export interface IConfirmEmail extends IEmail{
  verifyCode: string;
  hashCode: string;
}