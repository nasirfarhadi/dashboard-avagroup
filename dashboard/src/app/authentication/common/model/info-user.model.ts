import { IEmail } from './email-user.model';
export interface IUser extends IEmail {
  firstName: string;
  lastName: string;
  uniqueUserId: string;
  password: string;
  acceptCondition: boolean;
  hashCode: string;
}