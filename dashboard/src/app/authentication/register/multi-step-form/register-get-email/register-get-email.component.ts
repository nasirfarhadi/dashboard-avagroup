import { CoreService } from './../../../../core/common/service/core.service';
import { IEmail } from './../../../common/model/email-user.model';
import { RegisterService } from './../../../../core/common/service/register.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register-get-email',
  templateUrl: './register-get-email.component.html',
  styleUrls: ['./register-get-email.component.scss']
})
export class RegisterGetEmailComponent implements OnInit {
  emailForm: FormGroup;
  IEmail: IEmail;

  constructor(private registerService: RegisterService,
              private fb: FormBuilder,
              private coreService: CoreService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.emailForm = this.fb.group({
      email: ['',[Validators.required,Validators.email]]
    })
  }

  onSubmit(): void {
    this.coreService.showLoadingFunc(true);
    this.registerService.postEmail(this.emailForm.value).subscribe(
        result => {
          if(result.success){
            this.toastr.success(result.message);
            this.coreService.showLoadingFunc(false);
            // pass data result
            this.registerService.hashCode =  result.result.hashCode;
            this.registerService.verifyCodeLength =  result.result.verifyCodeLength;
            this.registerService.verifyCodeExpireTime_Sec =  result.result.verifyCodeExpireTime_Sec;
            
            // pass email and step form
            let valueEmail = this.emailForm.controls.email.value;
            this.registerService.emailUser = valueEmail;
            this.registerService.email = valueEmail;
            this.registerService.changeStep('step_2');
          } else{
            this.coreService.showLoadingFunc(false);
            this.toastr.error(result.message);
          }
        },
        (err) => console.log('err: ' + err)
      )

  }

}
