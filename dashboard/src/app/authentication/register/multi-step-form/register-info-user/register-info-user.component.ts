import { CoreService } from './../../../../core/common/service/core.service';
import { Router } from '@angular/router';
import { RegisterService } from './../../../../core/common/service/register.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register-info-user',
  templateUrl: './register-info-user.component.html',
  styleUrls: ['./register-info-user.component.scss']
})
export class RegisterInfoUserComponent implements OnInit {
  infoRegisterForm: FormGroup;
  public classChange: boolean = false;
  public confirmEmail;
  public hashCode= 'hvkbjnk.am/d';

  acceptCondition = [false, true];

  constructor(private fb: FormBuilder,
              private registerService: RegisterService,
              private router: Router,
              private coreService: CoreService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.infoRegisterForm = this.fb.group({
      firstName: ['',[Validators.required]],
      lastName: ['',[Validators.required]],
      uniqueUserId: ['',[
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(20),
          Validators.pattern('^[a-z]+[a-zA-Z0-9_]{4,19}$')
        ]],
      password: ['',[Validators.required,
        Validators.pattern(/[A-Z]/),
        Validators.pattern(/[a-z]/),
        Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}'),
        Validators.minLength(5)
          ]],
      acceptCondition: [false,Validators.required],
    })

    // get data resault step 1    
    this.hashCode=this.registerService.hashCode;

  }

  changeType(e){
    this.classChange = !this.classChange;
  }


  cancelReg(){
    this.router.navigate(['/login']);
  }

  onSubmitRegister(e){
    e.preventDefault();
    this.mapFormValuesToModel();
    this.coreService.showLoadingFunc(true);
    this.registerService.postInfoEmail(this.confirmEmail).subscribe(
      (response) => {
        if(!response.success){
          this.toastr.error(response.message);
          this.coreService.showLoadingFunc(false);
        } else {
          this.toastr.success(response.message);
          this.coreService.showLoadingFunc(false);
          // Navigate to the Login page
          this.router.navigate(['/login']);
        }
      },
      (err) => console.log('err: ' + err)
    )
  }

  mapFormValuesToModel(){
    this.confirmEmail = {
      email: this.registerService.email,
      hashCode: this.hashCode,
      firstName: this.infoRegisterForm.controls.firstName.value,
      lastName: this.infoRegisterForm.controls.lastName.value,
      uniqueUserId: this.infoRegisterForm.controls.uniqueUserId.value,
      password: this.infoRegisterForm.controls.password.value,
      acceptCondition: this.infoRegisterForm.controls.acceptCondition.value
    }
  }

}
