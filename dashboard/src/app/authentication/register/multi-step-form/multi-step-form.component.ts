import { RegisterService } from './../../../core/common/service/register.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-multi-step-form',
  templateUrl: './multi-step-form.component.html',
  styleUrls: ['./multi-step-form.component.scss']
})
export class MultiStepFormComponent implements OnInit, OnDestroy {

  public myStep: string;
  sampleSubscription: Subscription | null = null;

  constructor(private registerServices: RegisterService) { 
    
  }
  
  ngOnInit(): void {
    this.sampleSubscription = this.registerServices.telecast$.subscribe(message => {
      this.myStep = message;
    });
  }
  
  ngOnDestroy(): void {
    this.sampleSubscription.unsubscribe();
  }

}
