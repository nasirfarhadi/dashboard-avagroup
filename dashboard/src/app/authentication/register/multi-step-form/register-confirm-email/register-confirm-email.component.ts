import { CoreService } from './../../../../core/common/service/core.service';
import { IConfirmEmail } from './../../../common/model/confirm-email-user.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IEmail } from './../../../common/model/email-user.model';
import { RegisterService } from './../../../../core/common/service/register.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register-confirm-email',
  templateUrl: './register-confirm-email.component.html',
  styleUrls: ['./register-confirm-email.component.scss']
})
export class RegisterConfirmEmailComponent implements OnInit {

  confirmEmailForm: FormGroup;

  public counterSecund: number = 59;
  public counterMinute: number= 4;
  public counterDateAsStr: string;
  public showBtnSend = false;

  public maskSample= "-  ";
  public showMaskInput:string;
  
  public verifyCodeLength= 2;
  public verifyCodeExpireTime_Sec=500;
  public hashCode= 'hvkbjnk.am/d';

  public emailUser : IEmail;
  public confirmEmail;

  constructor(private registerService: RegisterService,
              private fb: FormBuilder,
              private coreService: CoreService,
              private toastr: ToastrService) { 
    this.emailUser = this.registerService.emailUser;
  }

  ngOnInit(): void {
    this.startCountdown();
    this.confirmEmailForm = this.fb.group({
      verifyCode: ['',[Validators.required]]
    }) 
    // get data resault step 1    
    this.verifyCodeLength= this.registerService.verifyCodeLength;
    this.verifyCodeExpireTime_Sec=this.registerService.verifyCodeExpireTime_Sec;
    this.hashCode=this.registerService.hashCode;
    this.secondsToHms(this.verifyCodeExpireTime_Sec);
    this.showMaskInput= this.maskSample.repeat(this.verifyCodeLength - 1)+ "-";
  }

  // convert second to minute and second
  secondsToHms(d) {
    d = Number(d);
    let m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);
    this.counterMinute = m;
    this.counterSecund = s;
  }

// timer resend confirm code
  startCountdown() {
    this.counterDateAsStr=this.counterMinute+":"+this.counterSecund;
    const interval = setInterval(() => {
      this.counterSecund--;
      if (this.counterSecund == 0 && this.counterMinute == 0){
        clearInterval(interval);
        this.showBtnSend = !this.showBtnSend;
        this.formatDisplayTime(true);
      }else if (this.counterSecund < 0 ) {
        this.counterSecund= 59;
        this.counterMinute--;
        this.formatDisplayTime();
      }else if (this.counterSecund < 10 ) {
        this.counterDateAsStr=this.counterMinute+":0"+this.counterSecund;
        this.formatDisplayTime(true);
      }else{
        this.formatDisplayTime();
      }
    }, 1000);
  }

  formatDisplayTime(hasZero:Boolean=false){
    this.counterDateAsStr=this.counterMinute+":"+(hasZero?"0":"")+this.counterSecund;
  }


  backStep(){
    this.registerService.changeStep('step_1');
  }

  onSubmitConfirm(e): void{
    e.preventDefault();
    this.confirmEmail = {
      email: this.registerService.email,
      hashCode: this.hashCode,
      verifyCode: this.confirmEmailForm.controls.verifyCode.value
    }
    this.coreService.showLoadingFunc(true);
    this.registerService.postConfirmEmail(this.confirmEmail).subscribe(
      (response :IConfirmEmail) => {
        if(response.success){
          this.toastr.success(response.message);
          this.coreService.showLoadingFunc(false);
           // change step          
           this.registerService.hashCode = response.result.hashCode;
           this.registerService.changeStep('step_3');
          } else{
            this.coreService.showLoadingFunc(false);
            this.toastr.error(response.message);
          }
      },
      (err) => console.log('err: ' + err)
    )
  }


  resendCode(e){
    e.preventDefault();
    this.coreService.showLoadingFunc(true);
    this.registerService.postEmail(this.emailUser).subscribe(
      result => {
        if(result.success){
          this.toastr.success(result.message);
          this.coreService.showLoadingFunc(false);
          // pass data result
          this.registerService.hashCode =  result.result.hashCode;
          this.registerService.verifyCodeLength =  result.result.verifyCodeLength;
          this.registerService.verifyCodeExpireTime_Sec =  result.result.verifyCodeExpireTime_Sec;
          
        } else{
          this.coreService.showLoadingFunc(false);
          this.toastr.error(result.message);
        }
      },
      (err) => console.log('err: ' + err)
    )
  }


}
