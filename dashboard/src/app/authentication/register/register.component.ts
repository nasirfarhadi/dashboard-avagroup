import { Subscription } from 'rxjs';
import { CoreService } from './../../core/common/service/core.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  showLoading: boolean;
 sampleSubscription: Subscription;

  constructor(private coreService: CoreService) { }

  ngOnInit(): void {
    this.sampleSubscription = this.coreService.showLoadingCast$.subscribe(message => {
      this.showLoading = message;
    });
  }

}
