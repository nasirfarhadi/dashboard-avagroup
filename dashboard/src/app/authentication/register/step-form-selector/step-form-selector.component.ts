import { RegisterService } from './../../../core/common/service/register.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-step-form-selector',
  templateUrl: './step-form-selector.component.html',
  styleUrls: ['./step-form-selector.component.scss']
})
export class StepFormSelectorComponent implements OnInit {
  public myStep: string;
  sampleSubscription: Subscription | null = null;

  isActiveStep1:boolean = true;
  isActiveStep2:boolean = false;
  isActiveStep3:boolean = false;
  isCheckStep1:boolean = false;
  isCheckStep2:boolean = false;

  constructor(private registerServices:RegisterService ) { }

  ngOnInit(): void {
    this.sampleSubscription = this.registerServices.telecast$.subscribe(message => {
      this.myStep = message;
      this.changeStepFunc(this.myStep);
    });
  }

  changeStepFunc(e: string){
    if(e=='step_2'){
      this.isActiveStep1 = true;
      this.isCheckStep1 = true;
      this.isActiveStep2 = true
    } else if(e=='step_3'){
      this.isActiveStep2 = true;
      this.isActiveStep1 = true;
      this.isActiveStep3 = true;
      this.isCheckStep1 = true;
      this.isCheckStep2 = true;
    }
  }



}
