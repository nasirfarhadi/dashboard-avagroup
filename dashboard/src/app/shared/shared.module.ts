import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropDownDirective } from './directive/drop-down.directive';



@NgModule({
  declarations: [DropDownDirective],
  exports: [DropDownDirective],
  imports: [
    CommonModule
  ]
})
export class SharedModule {
   static forRoot(): ModuleWithProviders<SharedModule> {
    // Forcing the whole app to use the returned providers from the AppModule only.
    return {
      ngModule: SharedModule,
      providers: [ /* All of your services here. It will hold the services needed by `itself`. */]
    };
  }
 }
