import { Directive, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appDropDown]'
})
export class DropDownDirective {
  @HostBinding('class.show') isShow = false;

  constructor(private elm: ElementRef) {
  }

  @HostListener('click', ['$event']) toggleOpen(e: any) {
    e.preventDefault();
      this.isShow = !this.isShow;
  }
}
