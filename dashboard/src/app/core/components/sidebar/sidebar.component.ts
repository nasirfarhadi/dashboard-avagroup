import { CoreService } from './../../common/service/core.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(public coreService: CoreService) { }

  ngOnInit(): void {
  }

}
