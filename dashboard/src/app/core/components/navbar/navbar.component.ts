import { TokenService } from './../../common/service/token.service';
import { ILanguage } from './../../common/model/language.model';
import { Subscription } from 'rxjs';
import { IValueToken } from './../../common/model/valueToken.model';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/common/service/auth.service';
import { CoreService } from './../../common/service/core.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit,OnDestroy {
  imgPath: string = './assets/img/iran.png';
  langText: string = 'فارسی';

  public languages: ILanguage[];

  uniqueUserId: string;
  logoutToken: IValueToken;

  userInfoUnSubscribe: Subscription;

  constructor(public coreService: CoreService,
              private authSrvice: AuthService,
              private tokenService: TokenService,
              private translateService: TranslateService,
              private router: Router) { }
 

  ngOnInit(): void {
    this.userInfoUnSubscribe =  this.authSrvice.getUserInfo().subscribe(
      (response) =>{        
        this.uniqueUserId = response.result.accountInfo.uniqueUserId;
        if(!response.success){
          this.authSrvice.doLogoutUser();
        }
      },
      (err) => console.log('error get:' + err)
    );
    this.switchLanguage(this.coreService.defaultLocaleLang);

     this.languages = this.coreService.getLanguage();

  }

  ngOnDestroy(): void {
    this.userInfoUnSubscribe.unsubscribe();
  }



  switchLanguage(language: string) {
    this.translateService.use(language);
    if(language === 'en') {
       this.imgPath = "./assets/img/us-flag.svg"
       this.langText= "English"
       localStorage.setItem("selectedLang", 'en');
      } else if(language === 'fa') {
        localStorage.setItem("selectedLang", 'fa');
        this.imgPath = "./assets/img/iran.png";     
        this.langText= "فارسی";
    }
  }


  logout(){
    this.logoutToken = {
      value: this.tokenService.getRefreshToken()
    }
    this.authSrvice.logout(this.logoutToken).subscribe(response => {
      if (response) {
        this.router.navigate(['/login']);
      }
    });
  }

}
