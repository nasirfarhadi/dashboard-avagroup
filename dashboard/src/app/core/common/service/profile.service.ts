import { CoreService } from './core.service';
import { IAvatar } from './../model/avatar.model';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/common/service/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { APP_CONFIG, IAppConfig } from '../config/app.Config';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(@Inject(APP_CONFIG) public appConfig: IAppConfig,
              private http: HttpClient,
              private authService: AuthService,
              private router: Router,
              private coreService: CoreService) { }


  updateProfileAvatar(formData): Observable<IAvatar>{
    return this.http.put<any>(`${this.appConfig.apiEndpoint}${this.appConfig.profileAvatar}`, formData,{
      headers : new HttpHeaders({
        'Accept-Language': this.coreService.getLocalLanguage()
      })
    }).pipe(
      tap((data) => 
      {
        console.log(JSON.stringify(data));
      }),
      catchError(error => {
        console.log('updateProfileAvatar error' + error.error);
        this.authService.doLogoutUser();
        this.router.navigate(["/login"]);
        return of(false);
      })
    );
  }


  getProfileAvatar(width:number): Observable<Blob>{
    return this.http.get(`${this.appConfig.apiEndpoint}${this.appConfig.profileShowAvatar}/${width}`,{
      headers: new HttpHeaders({
        'accept': '*/*',
        'Accept-Language': this.coreService.getLocalLanguage()
      }),
      responseType: "blob"
    });
  }

}
