import { AlertMessageService } from './alert-message.service';
import { CoreService } from './core.service';
import { IUser } from './../../../authentication/common/model/info-user.model';
import { IConfirmEmail } from './../../../authentication/common/model/confirm-email-user.model';
import { IEmail } from './../../../authentication/common/model/email-user.model';
import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { APP_CONFIG, IAppConfig } from '../config/app.Config';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  emailUser: IEmail;

  verifyCodeLength:number;
  verifyCodeExpireTime_Sec:number;
  hashCode:string;
  email:string;
  
  private stepRegister = new BehaviorSubject<string>("step_1");
  telecast$ = this.stepRegister.asObservable();


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept-Language': this.coreService.getLocalLanguage()
    })
  };


  constructor(@Inject(APP_CONFIG) public appConfig: IAppConfig,
    private http: HttpClient,
    private coreService: CoreService,
    private alertMessageService: AlertMessageService) {
   }

  changeStep(step: string) {
    this.stepRegister.next(step);
  }

  postEmail(email: IEmail){
    return this.http.post<IEmail>(`${this.appConfig.apiEndpoint}${this.appConfig.registerBaseUrl}${this.appConfig.sendUrl}`, email, this.httpOptions)
      .pipe(
        tap(
          response =>{
            response || {}
          } 
        ),
        catchError(this.alertMessageService.handleError)
      )

  }


  postConfirmEmail(model: IConfirmEmail): Observable<IConfirmEmail>{
    return this.http.post<IConfirmEmail>(`${this.appConfig.apiEndpoint}${this.appConfig.registerBaseUrl}${this.appConfig.verifyUrl}`, model, this.httpOptions)
      .pipe(
        tap( 
          response => response || {}
        ),
        catchError(this.alertMessageService.handleError)
      )
  }

  postInfoEmail(model: IUser): Observable<IConfirmEmail>{
    return this.http.post<IConfirmEmail>(`${this.appConfig.apiEndpoint}${this.appConfig.registerUrl}`, model, this.httpOptions)
      .pipe(
        tap( 
          response => response || {}
        ),
        catchError(this.alertMessageService.handleError)
      )
  }

}
