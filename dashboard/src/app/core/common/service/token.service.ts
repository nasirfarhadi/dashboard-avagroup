import { Injectable } from '@angular/core';
import { Credentials } from '../model/credential.model';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private readonly AccessToken = 'AccessToken';
  private readonly RefreshToken = 'RefreshToken';
  private readonly ExpireDateToken = 'ExpireDateToken';

  constructor() { }
  
  
  getAccessTokenExpirationDateUtc(): boolean{
    const expireDateLocal = this.getExpireDate()
    const expireDate = new Date(expireDateLocal);
    let currentTimeInSeconds=Math.floor(Date.now()/1000);
    let expireSeconds=Math.floor(+expireDate/1000);
    if (expireSeconds === currentTimeInSeconds) {
      return true;
    }
    return false
  }

  isAccessTokenTokenExpired(): boolean {
    const expirationDateUtc = this.getAccessTokenExpirationDateUtc();
    if (!expirationDateUtc) {
      return true;
    }
    return false;
  }



  getJwtToken() {
    return localStorage.getItem(this.AccessToken);
  }


  getRefreshToken() {
    return localStorage.getItem(this.RefreshToken);
  }

  getExpireDate() {
    return localStorage.getItem('ExpireDateToken');
  }

  storeJwtToken(jwt: string) {
    localStorage.setItem(this.AccessToken, jwt);
  }


  storeTokens(tokens: Credentials) {
    localStorage.setItem(this.AccessToken, tokens.result.access_token);
    localStorage.setItem(this.RefreshToken, tokens.result.refresh_token);
    localStorage.setItem(this.ExpireDateToken, tokens.result.expireDateTime);
  }

  removeTokens(){
    localStorage.removeItem(this.AccessToken);
    localStorage.removeItem(this.RefreshToken);
    localStorage.removeItem(this.ExpireDateToken);
  }

}

