import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ILanguage } from './../model/language.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject,Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  public navbarOpen: boolean = false;
  public defaultLocaleLang: string = 'en';

  subject: Subject<ILanguage[]> = new Subject();  


   langSite: ILanguage[] = [
    {
      id: 0,
      slug: 'fa',
      text: 'فارسی',
      image: './assets/img/iran.png'
   },
   {
      id:1,
      slug: 'en',
      text: 'English',
      image: './assets/img/us-flag.svg'
    }
  ]

  private showLoading = new BehaviorSubject<boolean>(false);
  showLoadingCast$ = this.showLoading.asObservable();

  constructor(private http: HttpClient) {
    // console.log('lan:' + JSON.stringify(this.langSite[0]))
   }

  showLoadingFunc(show: boolean) {
    this.showLoading.next(show);
  }

  sidebarShow(): void{
    this.navbarOpen = true;
  }

  sidebarHide(): void{
    this.navbarOpen = false;
  }
  

  // language

  getLanguage(){
    return this.langSite.slice();
  }
  // getLanguageItem(id: number){
  //   return this.langSite[id]
  // }


  getLocalLanguage(){
    return localStorage.getItem('selectedLang');
  }

}
