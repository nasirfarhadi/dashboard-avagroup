import { TokenService } from './token.service';
import { Injectable } from "@angular/core";
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})

export class AuthGuard implements CanActivate{
    constructor(private authService: AuthService,
                private router: Router,
                private tokenService: TokenService){

    }

    canActivate() {
        return this.canLoad();
      }
    
    canLoad() {
    if (!this.authService.isLoggedIn()) {
        this.tokenService.removeTokens();
        this.router.navigate(['/login']);
    }
    return this.authService.isLoggedIn();
    }
}