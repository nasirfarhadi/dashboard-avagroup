import { IAppConfig } from './../config/app.Config';
import { TokenService } from './token.service';
import { CoreService } from './core.service';
import { Router } from '@angular/router';
import { IValueToken } from './../model/valueToken.model';
import { mapTo, tap, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Credentials } from '../model/credential.model';
import { IDataInfoUser } from '../model/data-info-user.model';
import { APP_CONFIG } from '../config/app.Config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

 
  private loggedUser: string;
  infoUser: any;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept-Language': this.coreService.getLocalLanguage()
    })
  };

  constructor(@Inject(APP_CONFIG) public appConfig: IAppConfig,
              private http: HttpClient,
              private router: Router,
              private coreService: CoreService,
              private tokenService: TokenService) { }

  login(user:Credentials): Observable<boolean>{
    return this.http.post<Credentials>(`${this.appConfig.apiEndpoint}${this.appConfig.login}`, user,this.httpOptions)
            .pipe(
              tap(tokens =>{
                if(tokens.success){
                  this.doLoginUser(user.username, tokens);
                }
                console.log('Tokens:' + JSON.stringify(tokens))
              }  
              ),
              mapTo(true),
              catchError(error => {
                alert(error.error);
                return of(false);
              })
            )
  }

  logout(token: IValueToken):Observable<IValueToken> {
    return this.http.post<any>(`${this.appConfig.apiEndpoint}${this.appConfig.logout}`,{
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept-Language': this.coreService.getLocalLanguage()
    })
  }).pipe(
      tap((res) => {
        console.log('log:' + res)
        this.doLogoutUser();
      }),
      catchError(error => {
          alert(error.error);
          return of(false);
      })
    )
  }

  isLoggedIn() {
      return !!this.tokenService.getJwtToken() && this.tokenService.isAccessTokenTokenExpired();
  }


  // refreshToken() {
  //   const refreshToken = this.getRefreshToken()
  //   return this.http.post<any>(`${config.apiUrl}${config.refreshToken}`, refreshToken)
  //   .pipe(tap((tokens: Credentials) => {
  //     if(tokens.success){
  //       console.log("refresh")
  //       this.storeJwtToken(tokens.result.access_token);
  //     }
  //   }))
  // }


  getUserInfo(): Observable<IDataInfoUser>{
    return this.http.get<any>(`${this.appConfig.apiEndpoint}${this.appConfig.profileUserInfo}`,{
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept-Language': this.coreService.getLocalLanguage()
      })
    }).pipe(
      tap((data) => 
      {
        // console.log(JSON.stringify(data))
      }),
      catchError(error => {
        console.log('bljn' + error.error);
        this.doLogoutUser();
        this.router.navigate(["/login"])
        return of(false);
      })
    )
  }






  doLoginUser(username: string, tokens: Credentials){
    this.loggedUser = username;
    this.tokenService.storeTokens(tokens);
  }

  doLogoutUser() {
    this.loggedUser = null;
    this.tokenService.removeTokens();
  }

}
