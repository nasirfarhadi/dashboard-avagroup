import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

 
interface System {
  import(request: string): Promise<any>;
}


declare var System: System;

export class WebpackTranslateLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return fromPromise(System.import(`../../../../assets/i18n/${lang}.json`)); // your path
  }
}