export interface IDataInfoUser{
    success:boolean;
    message:string;
    result:{
        accountInfo:{
            firstName:string;
            lastName:string;
            uniqueUserId:string;
            userName:string;
            email:string;
            phoneNumberConfirmed:boolean;
            personalVerified:boolean;
            addressVerified:boolean;
            hasAvatar:boolean;
        }
    }
}