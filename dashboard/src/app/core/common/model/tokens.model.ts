export class Tokens {
    success: boolean;
    message: string;
    result: IResultToken;
  }

export interface IResultToken{
  access_token: string;
  refresh_token: string;
  expireDateTime: string;
}