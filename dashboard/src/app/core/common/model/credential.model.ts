import { Tokens } from './tokens.model';
export interface Credentials extends Tokens {
  username: string;
  password: string;
}