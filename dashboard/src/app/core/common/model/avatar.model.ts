export interface IAvatar{
  photo:string;
  success: boolean;
  message: string;
}