export interface ILanguage {
    id:number;
    slug:string;
    text:string;
    image:string;
}