import { InjectionToken } from "@angular/core";

export let APP_CONFIG = new InjectionToken<string>("AppConfig");

export interface IAppConfig {
  apiEndpoint: string;
  registerBaseUrl: string,
  sendUrl: string,
  verifyUrl: string,
  registerUrl: string,
  login: string,
  logout: string,
  refreshToken: string,
  profile: string,
  profileUserInfo: string,
  profileAvatar: string,
  profileShowAvatar: string,
  userTradingAccount: string,
  userTradingAccountFilter: string
}

export const AppConfig: IAppConfig = {
  apiEndpoint: "http://pfa-my.ngproducts.net/api/v1/",
      // register path
      registerBaseUrl: 'Account/Register/',
      sendUrl: 'Code/Send',
      verifyUrl: 'Code/Verify',
      registerUrl: 'Account/Register/',
      // login Path
      login: 'Account/Login',
      logout: 'Account/Logout',
      refreshToken: 'Account/RefreshToken',
      //profile path
      profile: 'Profile',
      profileUserInfo: 'Profile/UserInfo',
      profileAvatar: 'Profile/Avatar',
      profileShowAvatar: 'Profile/ShowAvatar',
  
      // traiding path
      userTradingAccount: 'UserTradingAccount',
      userTradingAccountFilter: 'UserTradingAccount/Filter/1/100'
};