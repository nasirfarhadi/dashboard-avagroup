import { APP_CONFIG, IAppConfig } from './../config/app.Config';
import { CoreService } from './../service/core.service';
import { TokenService } from './../service/token.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { IValueToken } from './../model/valueToken.model';
import { Inject, Injectable } from '@angular/core';
import { AuthService } from 'src/app/core/common/service/auth.service';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpErrorResponse, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, of } from 'rxjs';
import { catchError, switchMap, filter, take, tap } from 'rxjs/operators';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(@Inject(APP_CONFIG) public appConfig: IAppConfig,
    public authService: AuthService,
    private http: HttpClient,
    private router: Router,
    private toaster: ToastrService,
    private tokenService: TokenService,
    private coreService: CoreService){}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept_Language': this.coreService.getLocalLanguage()
    })
  };
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.tokenService.getJwtToken()) {
      request = this.addToken(request, this.tokenService.getJwtToken());
    }
    // setTimeout(() => {
    //   this.handle401Error(request, next);
    // }, 20000);

    return next.handle(request).pipe(catchError(error => {
      if (error instanceof HttpErrorResponse && error.status === 401) {
        return this.handle401Error(request, next);
      }  else if ( error.status === 403){
        this.router.navigate['/not-found']
      } else if ( error.status === 503){
        this.toaster.error('در سرور خطایی رخ داده است  . دوباره تلاش کنید')
      }else if ( error.status === 505){
        this.toaster.error('در سرور خطایی رخ داده است  . دوباره تلاش کنید')
      }else if ( error.status === 405){
        this.toaster.error('در سرور خطایی رخ داده است  . دوباره تلاش کنید')
      } else {
        return throwError(error);
      }
    }));
  
  }

  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        'Authorization' : `Bearer ${token}`
      }
    });
  }

  private async handle401Error(request: HttpRequest<any>, next: HttpHandler){
    if (this.isRefreshing === false) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);
      const valueRefreshToken: IValueToken ={
        value: this.tokenService.getRefreshToken()
      }
      
      return await this.http.post(`${this.appConfig.apiEndpoint}${this.appConfig.refreshToken}`, valueRefreshToken, this.httpOptions)
        .pipe(
          tap((token: any) => {
            if(token.success){
              this.isRefreshing = false;
              this.refreshTokenSubject.next(token.result.access_token);
              this.tokenService.storeTokens(token);
              return next.handle(this.addToken(request, token.result.access_token));
            } else{
              this.isRefreshing = false;
              this.tokenService.removeTokens();
              this.router.navigate['/login'];
            }
          }),
          catchError(error => {
            console.log('err refresh token:' + error)
            this.router.navigate['/login'];
            return of(false);
          })
        ).toPromise();
      // return this.authService.refreshToken().pipe(
      //   switchMap((token: any) => {
      //     console.log('token:' + JSON.stringify(token))
      //     this.isRefreshing = false;
      //     this.refreshTokenSubject.next(token.jwt);
      //     return next.handle(this.addToken(request, token.jwt))
      //   })
      // );
    } else {
      return this.refreshTokenSubject.pipe(
        filter(token => token != null),
        take(1),
        switchMap(jwt => {
          return next.handle(this.addToken(request, jwt));
        })
      );
    }
  }






}
