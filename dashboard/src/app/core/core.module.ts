import { SharedModule } from './../shared/shared.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoadingComponent } from './components/loading/loading.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';

import {TranslateModule } from '@ngx-translate/core';
import { WebpackTranslateLoader } from './common/service/translate';
import { BreadcrumbModule } from 'angular-crumbs';


@NgModule({
  declarations: [
    SidebarComponent,
    NavbarComponent,
    LoadingComponent,
    BreadcrumbComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    SharedModule,
    BreadcrumbModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useClass: WebpackTranslateLoader
      }
    })
  ],
  providers: [
  ],
  exports: [
    SidebarComponent,
    NavbarComponent,
    LoadingComponent,
    BreadcrumbComponent
  ]
})
export class CoreModule { 
  // constructor( @Optional() @SkipSelf() core: CoreModule) {
  //   if (core) {
  //     throw new Error("CoreModule should be imported ONLY in AppModule.");
  //   }
  // }
}
