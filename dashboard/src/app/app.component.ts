import { CoreService } from './core/common/service/core.service';
import { Component, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dashboard';
  constructor(private translateService: TranslateService,
              private coreService: CoreService,
              private renderer: Renderer2,
              private router: Router, 
              private activatedRoute: ActivatedRoute,){
      // Set fallback language
    translateService.setDefaultLang(this.coreService.defaultLocaleLang);
    this.coreService.defaultLocaleLang = localStorage.getItem('selectedLang');
    // Supported languages
    translateService.addLangs(['en', 'fa']); 
  }

  ngOnInit(){
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .pipe(map(() => this.activatedRoute))
      .pipe(map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }))
      .pipe(filter((route) => route.outlet === 'primary'))
      .pipe(mergeMap((route) => route.data))
      .subscribe((data) => this.updateBodyClass(data.bodyClass));
  }

  private updateBodyClass(customBodyClass?: string) {
    this.renderer.setAttribute(document.body, 'class', '');
    if (customBodyClass) {
      this.renderer.addClass(document.body, customBodyClass);
    }
  }

}
