import { TranslateLoader,TranslateModule } from '@ngx-translate/core';
import { TokenInterceptor } from './core/common/http/http-interceptor';
import { CoreModule } from './core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationModule } from './authentication/authentication.module';
import { SharedModule } from './shared/shared.module';
import { WelcomeComponent } from './view/welcome/welcome.component';
import { NotfoundpageComponent } from './view/notfoundpage/notfoundpage.component';
import { AuthGuard } from './core/common/service/auth.guard';
import { AuthService } from './core/common/service/auth.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { WebpackTranslateLoader } from './core/common/service/translate';
import { AppConfig, APP_CONFIG } from './core/common/config/app.Config';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NotfoundpageComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    AuthenticationModule,
    SharedModule.forRoot(),
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }), // ToastrModule added
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: WebpackTranslateLoader
      }
    })
  ],
  providers: [
    AuthGuard,
    AuthService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: APP_CONFIG, useValue: AppConfig }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
