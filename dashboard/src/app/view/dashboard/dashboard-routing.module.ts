import { NotfoundpageComponent } from './../notfoundpage/notfoundpage.component';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TraidingFormComponent } from './account/my-account/traiding/components/traiding-form/traiding-form.component';
import { TradingResolverService } from './account/my-account/traiding/common/service/trading-resolver.service';

const routes: Routes = [
  { path: "", component: DashboardComponent,
    children: [
    { 
      path: "home",component: HomeComponent,
      data: {
        breadcrumb: "Home",
       }
    },
    { 
      path: "account",
      data: {breadcrumb: 'account'},
      loadChildren: () =>
      import('./account/account.module').then(m => m.AccountModule) 
    },
    {
      path:'create-traiding', component: TraidingFormComponent,
      data: { 
        bodyClass: 'orange-color',
        breadcrumb: 'create-trading' 
      }
    },
    {
      path: 'create-traiding/:id/edit', component: TraidingFormComponent,
      data: { 
        bodyClass: 'orange-color',
        breadcrumb: 'edit-trading'
      },
      resolve: { resolvedData: TradingResolverService },
    },
    { 
      path: "",redirectTo: 'home', pathMatch: 'full' 
    },
    { 
      path: "**",component: NotfoundpageComponent
    }
   ]
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
