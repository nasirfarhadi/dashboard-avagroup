import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { CoreService } from './../../../../../core/common/service/core.service';
import { ProfileService } from './../../../../../core/common/service/profile.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/core/common/service/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit,OnDestroy {

  uniqueUserId: string;

  public showDialog =false;
  
  uploadForm: FormGroup;  
  imageURL: string = './assets/img/placeholderavatar.jpg';
  sanitizedImageBlobUrl: SafeUrl | null = './assets/img/placeholderavatar.jpg';

  messageSizePic:string;

  showLoading: boolean;
  sampleSubscription: Subscription;
  profileSubscription: Subscription;

  constructor(private authSrvice: AuthService,
              private profileService: ProfileService,
              private fb: FormBuilder,
              private coreService: CoreService,
              private toastr: ToastrService,
              private sanitizer: DomSanitizer) { }


  ngOnInit(): void {
    this.getUserInfo();
    this.uploadForm = this.fb.group({
      profile: ['', [Validators.required]]
    });
    this.getProfileAvatar(92);

    this.sampleSubscription = this.coreService.showLoadingCast$.subscribe(message => {
      this.showLoading = message;
    });
  }

  ngOnDestroy(): void {
    this.sampleSubscription.unsubscribe();
    this.profileSubscription.unsubscribe();
  }


  getUserInfo(){
    this.profileSubscription = this.authSrvice.getUserInfo().subscribe(
      (response) => {
        this.uniqueUserId = response.result.accountInfo.uniqueUserId;
        if(!response.success){
         this.authSrvice.doLogoutUser();
        }
      },
      (err) => console.log('error get:' + err)
    )
  }


  getProfileAvatar(width:number){
    this.profileService.getProfileAvatar(width).subscribe(
      imageDataBlob => {
        let objectURL = URL.createObjectURL(imageDataBlob);       
        this.sanitizedImageBlobUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      }
    )
  }






  

  onFileChanged(event) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.uploadForm.patchValue({
        profile: file
      });
      const Img = new Image();
      const filesToUpload = (event.target.files);
      Img.src = URL.createObjectURL(filesToUpload[0]);
      Img.onload = (e: any) => {
        const height = e.path[0].height;
        const width = e.path[0].width;
        if(height === width){
          this.messageSizePic= '';
          // File Preview
          const reader = new FileReader();
          reader.onload = () => {         
              this.imageURL = reader.result as string;              
          }
          reader.readAsDataURL(file);
        }else{
          this.uploadForm.patchValue({
            profile: ''
          });
            this.messageSizePic = 'MESSAGE.UPLOAD.SIZE';
        }
      }
    }
  }

  updateProfileAvatar(e){
    e.preventDefault();
    this.coreService.showLoadingFunc(true);
    const formData = new FormData();
    formData.append('photo', this.uploadForm.value.profile);
    formData.append('fileName', this.uploadForm.value.profile.name);
    this.profileService.updateProfileAvatar(formData).subscribe(
      (response) => {
        if(response.success){
          this.showDialog = !this.showDialog;
          this.imageURL = './assets/img/placeholderavatar.jpg';
          this.coreService.showLoadingFunc(false);
          this.toastr.success(response.message);
        }else{
          this.coreService.showLoadingFunc(false);
          this.toastr.error(response.message);
        }
      },
      (err) => console.log('err avatar put:' + JSON.stringify(err))
      )
  }


  openDialog(e){
    e.preventDefault();
    this.showDialog = !this.showDialog;
  } 


}
