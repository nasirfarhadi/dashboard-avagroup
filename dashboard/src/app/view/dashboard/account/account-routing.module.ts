import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotfoundpageComponent } from '../../notfoundpage/notfoundpage.component';
import { AccountComponent } from './account.component';

const routes: Routes = [
  { path: "", component: AccountComponent,
  children: [
    {
      path: "my-information",
      data: {breadcrumb: 'my-information'},
      loadChildren: () =>
      import('./my-information/my-information.module').then(m => m.MyInformationModule) 
    },
    {
      path: "my-account",
      data: {breadcrumb: 'my-account'},
      loadChildren: () =>
      import('./my-account/my-account.module').then(m => m.MyAccountModule) 
    },
    {
      path: "", redirectTo:"my-account", pathMatch: "full"
    },
    { 
      path: "**",component: NotfoundpageComponent
    }
   ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
