import { TraidingComponent } from './traiding.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotfoundpageComponent } from 'src/app/view/notfoundpage/notfoundpage.component';

const routes: Routes = [
  { path: "", component: TraidingComponent,
  children: [
    { 
      path: "**",component: NotfoundpageComponent
    }
   ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TraidingRoutingModule { }
