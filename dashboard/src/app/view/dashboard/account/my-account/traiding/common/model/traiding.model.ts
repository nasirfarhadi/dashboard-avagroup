export interface ITradingPost extends IMessage{
    id:number;
    cabinNumber: string;
    tradingAccount: string
    published: boolean;
}

export interface ITradingGet extends IMessage{
  result: {
    records: IRecordTrading[];
    totalCount: number;
    }
}

export interface ITradingSingleGet extends IMessage{
  result: IRecordTrading;
}

export interface IRecordTrading{
  id: number;
  cabinNumber: string;
  tradingAccount: string;
  published: boolean;
  locked: boolean;
  createdDateUtc: string;
}

export interface IMessage {
  success:boolean;
  message:string;
}

export interface ITradingResolved {
  Traiding: ITradingSingleGet;
  error?: any;
}