import { Subscription } from 'rxjs';
import { IRecordTrading } from './../../common/model/traiding.model';
import { Component, OnInit } from '@angular/core';
import { TraidingService } from '../../common/service/traiging.service';

@Component({
  selector: 'app-item-traiding',
  templateUrl: './item-traiding.component.html',
  styleUrls: ['./item-traiding.component.scss']
})
export class ItemTraidingComponent implements OnInit {

  tradingItem: IRecordTrading[];
  public showDialog =false;
  itemTradingSubscription:Subscription;

  constructor(private traidingService: TraidingService) { }

  ngOnInit(): void {
    this.getTrading();
  }

  ngOnDestroy(): void {
    this.itemTradingSubscription.unsubscribe();
  }

  deleteTrading(id: number){
   this.traidingService.deleteTrading(id).subscribe(
      (response) => {
        this.getTrading();
        this.openDialog();
      },
      (err) => console.log('dl err:', err)
    )
  }

  getTrading(){
    this.itemTradingSubscription = this.traidingService.getTraidingAccountInfo().subscribe(
      (response) => {
        if(response.success){
          this.tradingItem = response.result.records;
        }
      },
      (err) => console.log('err:' + err)
    )
  }

  trackByFunc(index: number, el:any){
    return el.id;
  }

  openDialog(){
    this.showDialog = !this.showDialog;
  } 

}
