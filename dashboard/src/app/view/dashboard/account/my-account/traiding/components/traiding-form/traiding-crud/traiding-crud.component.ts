import { ActivatedRoute, Router } from '@angular/router';
import { ITradingPost } from './../../../common/model/traiding.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TraidingService } from '../../../common/service/traiging.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-traiding-crud',
  templateUrl: './traiding-crud.component.html',
  styleUrls: ['./traiding-crud.component.scss']
})
export class TraidingCrudComponent implements OnInit {
  pageTitle: string;
  statusTrading: boolean = true;
  published = [false, true];
  traidingForm: FormGroup;
  createTraiding: ITradingPost;
  public showDialog =false;

  constructor(private fb: FormBuilder,
              private traidingService: TraidingService,
              private router: Router,
              private route: ActivatedRoute,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.traidingForm = this.fb.group({
      cabinNumber: ['',[Validators.required, Validators.maxLength(8)]],
      traidingAccount: ['', [Validators.required, Validators.maxLength(7)]],
      published:[true]
    })
    this.route.paramMap.subscribe(params => {
      const trdID =  +params.get('id');
      if(trdID) {
        this.pageTitle = 'EDITACCOUNT';
        this.resolveData();
        this.defaultTradingData();
        this.getTrading(trdID);
      }else{
        this.pageTitle = 'NEWACCOUNT';
        this.defaultTradingData();
      }
    });
  }


  getTrading(id: number){
    this.traidingService.getItemTriding(id).subscribe(
      (response) => {
        this.createTraiding.id = response.result.id;
        this.createTraiding.cabinNumber = response.result.cabinNumber;
        this.createTraiding.tradingAccount = response.result.tradingAccount;
        this.createTraiding.published = response.result.published;
      }
    )
  }


  onSubmit(event){
    event.preventDefault();
    this.mapFormValuesToTraidingModel();
    if(this.createTraiding.id){
      this.traidingService.updateTrading(this.createTraiding).subscribe({
        next: () => {
          console.log('cmt update trading: ' + JSON.stringify(this.createTraiding))
          this.reset();
            // Navigate back to the product list
          this.router.navigate(['dashboard/account']);
        },
        error: err => console.log('err crud:' + JSON.stringify(err))
      });
    } else {
      this.traidingService.creatTraiding(this.createTraiding).subscribe(
        (data) => {
          if(data.success){
            this.toastr.success(data.message);
            this.reset();
            // Navigate back to the product list
            this.router.navigate(['dashboard/account']);
          }else{
            this.toastr.error(data.message);
          }
        },
        (err) => console.log('err crud:' + JSON.stringify(err))
      )
    }

  }


  reset() {
    this.createTraiding = null;
  }

  mapFormValuesToTraidingModel(){
    this.createTraiding.cabinNumber= this.traidingForm.value.cabinNumber;
    this.createTraiding.tradingAccount= this.traidingForm.value.traidingAccount;
    this.createTraiding.published= this.traidingForm.value.published;
  }

  resolveData(){
    this.route.data.subscribe(data => {
      let resolvedData = data['resolvedData'].Traiding.result;
      this.traidingForm.patchValue({
        cabinNumber: resolvedData.cabinNumber,
        traidingAccount: resolvedData.tradingAccount,
        published: resolvedData.published
      });
      this.statusTrading = resolvedData.locked;
    });    
  }

  openDialog(e){
    e.preventDefault();
    this.showDialog = !this.showDialog;
  } 


  defaultTradingData(){
    this.createTraiding = {
      id: null,
      cabinNumber: null,
      tradingAccount:null,
      published: true,
      message:null,
      success:true
    }
  }

}
