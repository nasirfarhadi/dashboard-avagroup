import { WebpackTranslateLoader } from './../../../../../core/common/service/translate';
import { SharedModule } from './../../../../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './../../../../../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TraidingRoutingModule } from './traiding-routing.module';
import { TraidingComponent } from './traiding.component';
import { ItemTraidingComponent } from './components/item-traiding/item-traiding.component';
import { TraidingFormComponent } from './components/traiding-form/traiding-form.component';
import { TraidingCrudComponent } from './components/traiding-form/traiding-crud/traiding-crud.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [TraidingComponent, ItemTraidingComponent, TraidingFormComponent, TraidingCrudComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CoreModule,
    SharedModule,
    TraidingRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useClass: WebpackTranslateLoader
      }
    })
  ]
})
export class TraidingModule { }
