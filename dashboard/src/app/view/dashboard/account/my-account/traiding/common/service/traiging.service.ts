import { AlertMessageService } from './../../../../../../../core/common/service/alert-message.service';
import { CoreService } from './../../../../../../../core/common/service/core.service';
import { ITradingGet, ITradingPost, ITradingSingleGet } from './../model/traiding.model';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/common/service/auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { APP_CONFIG, IAppConfig } from 'src/app/core/common/config/app.Config';

@Injectable({
  providedIn: 'root'
})
export class TraidingService {

  constructor(@Inject(APP_CONFIG) public appConfig: IAppConfig,
              private http: HttpClient,
              private authService: AuthService,
              private router:Router,
              private coreService: CoreService,
              private alertMessageService: AlertMessageService) { }




  getTraidingAccountInfo(): Observable<ITradingGet>{
    return this.http.get<any>(`${this.appConfig.apiEndpoint}${this.appConfig.userTradingAccountFilter}`,{
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept-Language': this.coreService.getLocalLanguage()
      })
    }).pipe(
      tap((data) => 
      {
        // console.log(JSON.stringify(data))
      }),
      catchError(error => {
        console.log('getTraidingAccountInfo' + error.error);
        this.authService.doLogoutUser();
        this.router.navigate(["/login"]);
        return of(false);
      })
    )
  }



 getItemTriding(id: number): Observable<ITradingSingleGet> {
    return this.http.get<ITradingSingleGet>(`${this.appConfig.apiEndpoint}${this.appConfig.userTradingAccount}/${id}`,{
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept-Language': this.coreService.getLocalLanguage()
      })
    })
      .pipe(
        tap(data =>{
          // console.log('getTraiding: ' + JSON.stringify(data))
        }),
        catchError(this.alertMessageService.handleError)
      );
  }

  updateTrading(traiding:ITradingPost): Observable<ITradingPost> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json',
    'Accept-Language': this.coreService.getLocalLanguage()});
    return this.http.put<ITradingPost>(`${this.appConfig.apiEndpoint}${this.appConfig.userTradingAccount}/${traiding.id}`, traiding, { headers })
      .pipe(
        tap(() => console.log('updateProduct: ' + traiding.id)),
        // Return the product on an update
        map(() => traiding),
        catchError(this.alertMessageService.handleError)
      );
  }


  creatTraiding(traiding:ITradingPost): Observable<ITradingPost>{
    return this.http.post<ITradingPost>(`${this.appConfig.apiEndpoint}${this.appConfig.userTradingAccount}`,traiding,{
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept-Language': this.coreService.getLocalLanguage()
      })
    })
    .pipe(
      tap(data => {
        console.log('createTraiding:' + JSON.stringify(data))
      }),
      catchError(this.alertMessageService.handleError)
    )
  }


  deleteTrading(id: number): Observable<{}> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.delete<ITradingPost>(`${this.appConfig.apiEndpoint}${this.appConfig.userTradingAccount}/${id}`, { headers })
      .pipe(
        tap(data => console.log('deleteProduct: ' + id)),
        catchError(this.alertMessageService.handleError)
      );
  }


}
