import { ITradingResolved } from './../model/traiding.model';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TraidingService } from './traiging.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TradingResolverService implements Resolve<ITradingResolved>{

  constructor(private traidingService:TraidingService) { }

  resolve(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<ITradingResolved> {
   const id = route.paramMap.get('id');
   if(isNaN(+id)) {
    const message = `Trading id was not a number: ${id}`;
    console.error(message);
    return of({ Traiding: null, error: message});
   }

   return this.traidingService.getItemTriding(+id)
   .pipe(
     map(Traiding => ({ Traiding }),
     ),
     catchError(error => {
       const message = `Retrieval error: ${error}`;
       console.error(message);
       return of({ Traiding: null, error: message });
     })
   );



  }
}
