import { EMoneyComponent } from './e-money.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotfoundpageComponent } from 'src/app/view/notfoundpage/notfoundpage.component';

const routes: Routes = [
  { path: "", component: EMoneyComponent,
  children: [
    { 
      path: "**",component: NotfoundpageComponent
    }
   ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EMoneyRoutingModule { }
