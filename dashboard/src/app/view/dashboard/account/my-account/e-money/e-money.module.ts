import { WebpackTranslateLoader } from './../../../../../core/common/service/translate';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EMoneyRoutingModule } from './e-money-routing.module';
import { EMoneyComponent } from './e-money.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [EMoneyComponent],
  imports: [
    CommonModule,
    EMoneyRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useClass: WebpackTranslateLoader
      }
    })
  ]
})
export class EMoneyModule { }
