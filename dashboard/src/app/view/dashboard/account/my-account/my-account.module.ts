import { WebpackTranslateLoader } from './../../../../core/common/service/translate';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyAccountRoutingModule } from './my-account-routing.module';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MyAccountComponent } from './my-account.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [SidebarComponent, MyAccountComponent],
  imports: [
    CommonModule,
    MyAccountRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useClass: WebpackTranslateLoader
      }
    })
  ]
})
export class MyAccountModule { }
