import { MyAccountComponent } from './my-account.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotfoundpageComponent } from 'src/app/view/notfoundpage/notfoundpage.component';

const routes: Routes = [
  { path: "", component: MyAccountComponent,
  children: [
    { 
      path: "traiding",
      data: {breadcrumb: 'trading'},
      loadChildren: () =>
      import('./traiding/traiding.module').then(m => m.TraidingModule) 
    },
    { 
      path: "e-money",
      data: {breadcrumb: 'e-money'},
      loadChildren: () =>
      import('./e-money/e-money.module').then(m => m.EMoneyModule) 
    },
    {
      path: "", redirectTo:"traiding", pathMatch: "full"
    },
    { 
      path: "**",component: NotfoundpageComponent
    }
   ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyAccountRoutingModule { }
