import { WebpackTranslateLoader } from './../../../core/common/service/translate';
import { CoreModule } from './../../../core/core.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [AccountComponent, ProfileComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CoreModule,
    AccountRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useClass: WebpackTranslateLoader
      }
    })
  ]
})
export class AccountModule { }
