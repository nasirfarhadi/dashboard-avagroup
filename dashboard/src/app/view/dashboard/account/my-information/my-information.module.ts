import { WebpackTranslateLoader } from './../../../../core/common/service/translate';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyInformationRoutingModule } from './my-information-routing.module';
import { MyInformationComponent } from './my-information.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [MyInformationComponent],
  imports: [
    CommonModule,
    MyInformationRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useClass: WebpackTranslateLoader
      }
    })
  ]
})
export class MyInformationModule { }
