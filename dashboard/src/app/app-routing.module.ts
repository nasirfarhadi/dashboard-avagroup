import { NotfoundpageComponent } from './view/notfoundpage/notfoundpage.component';
import { WelcomeComponent } from './view/welcome/welcome.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/common/service/auth.guard';

const routes: Routes = [
  {path: 'welcome',
    data: {
      breadcrumb: "Welcome"
   },
   component: WelcomeComponent,
   canActivate: [AuthGuard],
   canLoad: [AuthGuard] 
  },
  { path: "dashboard",
   data: {breadcrumb: "Dashboard"},
   canActivate: [AuthGuard],
   canLoad: [AuthGuard],
   loadChildren: () =>
          import('./view/dashboard/dashboard.module').then(m => m.DashboardModule) 
   },
  {path: '',redirectTo:'dashboard', pathMatch: 'full'},
  {path: '**', component: NotfoundpageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
